const path = require("path");
const fs = require("fs-extra");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const srcDir = path.resolve(__dirname, "src/wxWork");
const readDir = fs.readdirSync(srcDir);
console.log(`webpackcicd************************`);
console.log(`NODE_ENV:${process.env.NODE_ENV}`);
console.log(`CI_COMMIT_AUTHOR:${process.env.CI_COMMIT_AUTHOR}`);
console.log(`CI_COMMIT_MESSAGE:${process.env.CI_COMMIT_MESSAGE}`);
console.log(`CI_COMMIT_TITLE:${process.env.CI_COMMIT_TITLE}`);
console.log(`CI_COMMIT_REF_NAME:${process.env.CI_COMMIT_REF_NAME}`);
let entry = {};
readDir.forEach((item) => {
  let entryName = item.split(".")[0];
  entry[entryName] = path.resolve(srcDir, item);
});

module.exports = {
  mode: "development",
  entry,
  output: {
    filename: "[name].src.js",
    // path: path.resolve(__dirname, "dist/wxWork"),
    path: path.resolve("E:", "/testCicd/dist/wxWork"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        include: [srcDir],
        options: {
          presets: ["@babel/preset-env"],
          plugins: ["@babel/plugin-transform-runtime"],
        },
      },
    ],
  },
  plugins: [new CleanWebpackPlugin()],
  watchOptions: {
    ignored: /^((?!wxWork).)*$/,
  },
  devtool: "cheap-module-source-map",
};
